package main

import "fmt"

type Cliente struct {
	Nombre    string
	Apellidos string
	Direccion string
	Poliza    string
}

func main() {
	saludo := "Hola mundo"
	fmt.Println(saludo)

	if RetornarEstado() {
		fmt.Println("verdadero")
	} else {
		fmt.Println("falso")
	}
}

func RetornarEstado() bool {
	return false
}
